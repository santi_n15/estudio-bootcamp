export {}

// Path: src/bases/01-types.ts

// Tipos basicos en typescript

export const nombre: string = 'Naruto';

export const textoMultiLinea: string = `Esto es un texto
con distintos saltos de linea
y que se puede concatenar con el signo de suma (+)`;

