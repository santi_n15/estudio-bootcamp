// INTERFACES DE OBJETOS
interface Student {
    name: string;
    age: number;
    address: string;
}

export const ninja: Student = {
    name: 'Naruto',
    age: 20,
    address: 'Konoha'
}