// Clase ESTUDIANTE

import axios from "axios";

export class Student {
    private id: number;
    public name: string;
    public age: number;
    public address: string;

    constructor (id: number, name: string, age: number, address:string) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    get ProfileImg(): string {
        return `https://randomuser.me/api/portraits/men/${this.id}.jpg`;
    }

    //Metodo asincrono
    async getInfo(): Promise<string>{
        const respuestaFetch = fetch(`https://pokeapi.co/api/v2/pokemon/4`);
        
        const respuestaAxios = axios.get('https://pokeapi.co/api/v2/pokemon/4')

        console.log((await respuestaAxios).data);

        return '';
    }
}

export const joe = new Student(2, 'Joe', 21, 'California');

console.log(joe);

// Clase POKEMON
export class Pokemon {
    
    get getPokemonData(){
        const respuesta = `https://pokeapi.co/api/v2/pokemon/4`

        return respuesta;
    }

    // manera corta de declarar atributos
    constructor(
        public name: string,
        public type: string,
        public level: number
    ) {
        
    }
    
}