// DECORADORES

// Los decoradores son funciones que se ejecutan cuando se define una clase,
// y su objetivo es agregar funcionalidad a la clase.

// Los decoradores se definen con el símbolo @, y se colocan antes de la
// definición de la clase.

// Los decoradores pueden recibir parámetros, y pueden ser usados para
// modificar la clase, agregar propiedades, etc.

export   const myDecorator = (target: any) => {
    console.log(target);
}


class NewPokemon {
    constructor(
        public readonly id: number,
        public name: string,
    ) {}

    scream() {
        console.log(`NO QUIERO!!`);
    }

    speak() {
        console.log(`NO QUIERO HABLAR!!`);
    }
}



const MyDecorator = () => {
    return ( target: Function ) => {
        // console.log(target)
        return NewPokemon;
    }
}



@MyDecorator()
export class Pokemon {

    constructor(
        public readonly id: number,
        public name: string,
    ) {}

    scream() {
        console.log(`${ this.name.toUpperCase() }!!`)
    }

    speak() {
        console.log(`${ this.name }, ${ this.name }!`)
    }

}

export const charmander = new Pokemon(4, 'Charmander');

charmander.scream();
charmander.speak();

