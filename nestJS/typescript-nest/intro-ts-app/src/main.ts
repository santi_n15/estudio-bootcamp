import './style.css'
import typescriptLogo from './typescript.svg'
import { setupCounter } from './counter'
import {nombre, textoMultiLinea} from './bases/01-types';
import {joe, } from './bases/03-clases'

document.querySelector<HTMLDivElement>('#app')!.innerHTML = `
  <div>
    <h1>Buenas, soy ${nombre}</h1>
    <h2>Esta es un intro a conceptos typescript</h2>

    //Simulacion de array de objetos 
    <strong><h3>Informacion de los estudiantes: [</h3></strong>
    <p>Nombre: ${joe.name}</p>
    <p>Edad: ${joe.age}</p>
    <p>Direccion: ${joe.address}</p>
    <a href=${joe.ProfileImg}> Link a su perfil</a>
    <strong><h3>]</h3></strong>

    <h3>Asincronia - informacion obtenida desde otro lugar: <h3>
    <p>${joe.getInfo()}</p>

    <p>${textoMultiLinea}</p>

    <a href="https://vitejs.dev" target="_blank">
      <img src="/vite.svg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://www.typescriptlang.org/" target="_blank">
      <img src="${typescriptLogo}" class="logo vanilla" alt="TypeScript logo" />
    </a>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
    <p class="read-the-docs">
      Click on the Vite and TypeScript logos to learn more
    </p>
  </div>
`

setupCounter(document.querySelector<HTMLButtonElement>('#counter')!)
