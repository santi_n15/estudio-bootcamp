# Typescript

## puesta a punto en este ``superSet`` o ``superConjunto`` de javascript centrado en el uso que le vamos a dar con NEST

## Conceptos generales

- Bases del desarrollo con typescript
- Inyeccion de dependencias
- Tipos básicos

- Interfaces

- Implementaciones

- Clases

- Patrón adaptador

- Principio de sustitución de Liskov

- Inyección de dependencias

- Getters

- Métodos asíncronos

- Decoradores de clases y métodos


## Author

- Santiago neira
