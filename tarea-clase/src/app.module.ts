import { Module } from '@nestjs/common';
import { AutosModule } from './autos/autos.module';
import { MarcasModule } from './marcas/marcas.module';


// referencia a todo lo que es mi aplicacion. 
@Module({
  imports: [AutosModule, MarcasModule],
  providers: [],
  controllers: [],
  exports: [],
})

// Es simplemente una clase que tiene un decorador que le dice a Nest que es un modulo
export class AppModule {}
