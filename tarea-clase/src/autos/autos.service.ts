import { Injectable } from '@nestjs/common';
import { Auto } from './interfaces/auto.interface';
import  {v4 as uuid} from "uuid";

//En el servicio tenemos nuestra logica de negocios
@Injectable()
export class AutosService {
    // Voy a tener una lista de autos, y cada Auto debe estar conformado de la manera que esta definido en la interfaz
    private autos: Auto[] = [
        {
            id: uuid(),
            marca: 'Ford',
            modelo: 'Fiesta',
        },
        {
            id: uuid(),
            marca: 'Ford',
            modelo: 'Focus',
        },
        {
            id: uuid(),
            marca: 'Ford',
            modelo: 'Mustang',
        },

    ];

    // metodos de nuestro servicio, todo lo necesarios para abastecer a nuestro controlador
    findAll(){
        // Necesitamos acceder desde dentro si nuestro atributo es privado
        return this
    }

    findOne(id: string) {
        return this.autos.find(item => item.id === id)
    }

    findOneById(id: string) {
        const autoEncontrado = this.autos.find(item => item.id === id);

        if (!autoEncontrado)
            throw new Error('Auto no encontrado');
        else
            return autoEncontrado;
    }

}
