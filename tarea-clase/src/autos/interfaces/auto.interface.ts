

// Que atributos debe tener un auto
export interface Auto {
    // id: number;
    // UUID genera strings
    id: string;
    marca: string;
    modelo: string;
}