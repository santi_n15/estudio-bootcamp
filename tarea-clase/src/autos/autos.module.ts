import { Module } from '@nestjs/common';
import { AutosController } from './autos.controller';
import { AutosService } from './autos.service';

@Module({
  controllers: [AutosController],
  // Todos los servicios son proveedores
  // No todos los proveedores son servicios
  providers: [AutosService]
})
export class AutosModule {}
