// Que quiero recibir para crear un nuevo auto?
// Los dto son clases para controlar la informacion al momento de recibirla
export class CreateCarDto {
    // Estoy recibiendo un string  no quiero que cambie
    readonly marca: string;
}