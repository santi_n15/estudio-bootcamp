import { Body, Controller, Get, Param, ParseUUIDPipe, Patch, Post, UsePipes, ValidationPipe} from '@nestjs/common';
import { AutosService } from './autos.service';
import { CreateCarDto } from './dto/create-car.dto';


@Controller('autos')
export class AutosController {
    // no es la funcion de un controlador servir informacion
    // es la funcion de un servicio

    // private autos = ['Auto de agustin', 'Auto de maria', 'Auto de juan'];

    // inyectamos el servicio
    constructor(private readonly autosService: AutosService) {

    }
    @Get() 
    findAll() {
        return this.autosService.findAll();
    }

    //agregamos un parametro a la ruta
    @Get(':id')
    // usemos pipes ya creados para validar el parametro
    // Sino es un uuid ni siquiera va al servicio que es el que se va a comunicar con la base de datos
    findOne(@Param('id', ParseUUIDPipe) id: string) {
        console.log(id);
        return this.autosService.findOneById(id);
    }

    @Post()
    @UsePipes (ValidationPipe)
    crearAuto(@Body() createCarDto:CreateCarDto) {
        return createCarDto;
    }

    @Patch(":id")
    updateCar(
        @Param('id') id: string, 
        @Body() body: any) 
        {
         return body;   
        }

    // @Delete(':id')
    // removeCar(@Param('id') id: string) {
    //     return this.autosService.remove(id);
    // }

}
