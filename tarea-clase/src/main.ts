import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function main() {
  // NestFactory.create() returns an INestApplication instance
  // basicamente crea una instancia de la aplicacion
  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
main();
