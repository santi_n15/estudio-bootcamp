import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { MongooseModule } from '@nestjs/mongoose';

//Controllers
import { PokemonController } from './pokemon/pokemon.controller';

@Module({
  imports: [
    
    // Servicio del contenido estático, front de mi app
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),

    // Conexión a la base de datos
    MongooseModule.forRoot('mongodb://localhost/nest')

  ],
  controllers: [PokemonController],
  providers: [],
})
export class ModuloApp {}