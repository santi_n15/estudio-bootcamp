import { NestFactory } from '@nestjs/core';
import { ModuloApp } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(ModuloApp);

  app.setGlobalPrefix('api')

  await app.listen(3000);

  // Prefijo para las rutas


}
bootstrap();
