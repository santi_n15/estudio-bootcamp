# Estudiando distintos conceptos en typescript

## de preferencia los que vamos a estar utilizando con Nest js en la construccion de apps backend

Typescript enfocado en el uso de nestJS:


* Tipos básicos
* Interfaces
* Implementaciones
* Clases
* Patrón adaptador
* Principio de sustitución de Liskov
* Inyección de dependencias
* Getters
* Métodos asíncronos
* Decoradores de clases y métodos



# Typescript es una herramienta con utilidad en tiempos de desarrollo, por tanto todas las construcciones y conceptos de los que hablemos no existen en javascript.

Por ejemplo, los genericos y las interfaces.

# Utilidad

Evitar usar el tipo ANY, a la vez que evitamos repetir codigo cuando hay cambio de tipos en los retornos, ejemplo generalizar cuando hay un retorno `Numero` y `String`.




## Autor

* Santiago Neira